Run `make` to run the pipeline.

Run `make clear` to delete output files.

An overview of the scripts:
|Script|Goal|Input|Output|
|---|---|---|---|
|`dist.py`|Compute either the rank (`0` flag) or rank-indel (`1` flag) distance.|Two genome files in adjacency format, plus optional flags `indl` (to compute rank-indel distance) and `diag` (to show parameters of breakpoint graph). E.g. `./dist.py genome1.txt genome2.txt 0 0`|Distance|
|`dist_mat.py`|Generate lower triangular distance matrix in CSV format|Blank separated input with 3 columns: genome 1, genome 2, and distance|Distance matrix in CSV file|
|`pairs.py`|Generate all pairwise combinations in `data/`|`data/` directory|Tab separated pairwise combinations|

The scripts `dsuf.py` and `extr.py` have auxiliary functions for `dist.py`.