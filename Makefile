DATA := data/

all: magna_dist_matrix.meg

magna_dist_matrix.meg: magna_dist_matrix.csv
	ntaxa=$(shell cat $< | awk 'NR>1 {print}' | wc -l); \
		preamble="#mega\n!Format DataType=Distance DataFormat=LowerLeft NTaxa=$${ntaxa};\n"; \
		echo $${preamble} > $@

	cat $< | awk -F ',' 'NR>1 {print "[" $$1 "] #" $$1}' >> $@
	echo "" >> $@

	head -n 1 $< | tr ',' ' ' | awk -F ' ' '{print "[" $$0 " ]"}' >> $@
	awk -F ',' 'NR>1 {$$1="["$$1"]"; print}' $< >> $@

magna_dist_matrix.csv: $(DATA)
	./pairs.py $< | parallel --colsep '\t' ./dist.py {} 0 0 | ./dist_mat.py - > $@

clear:
	rm -f magna_dist_matrix.csv magna_dist_matrix.meg comparisons.txt genomes.txt
